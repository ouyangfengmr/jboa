package cn.jboa.action;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.opensymphony.xwork2.ActionSupport;

import cn.jboa.common.Constants;
import cn.jboa.entity.CheckResult;
import cn.jboa.entity.ClaimVoucher;
import cn.jboa.entity.ClaimVoucherDetail;
import cn.jboa.entity.Employee;
import cn.jboa.service.ClaimVoucherService;

public class ClaimVoucherAction extends BaseAction{
	
	private ClaimVoucher claimVoucher;
	private List<ClaimVoucherDetail> detailList;
	private ClaimVoucherService claimVoucherService;
	private String status;
	private Date startDate;
	private Date endDate;
	private static Map<String, String> statusMap;
	static{
		statusMap=new LinkedHashMap<String, String>();
	}
	
	public String searchClaimVoucher(){
		String vlaimStatus=null;
		if (claimVoucher !=null) {
			vlaimStatus=claimVoucher.getStatus();
		}
		Employee emp=(Employee) getSession().get(Constants.AUTH_EMPLOYEE);
		pageSupport=claimVoucherService.getClaimVoucherPage(emp, vlaimStatus, startDate, endDate, pageNo, pageSize);
		return "list";
	}
	
	public String toAdd(){
		return "toAdd";
	}
	
	public String saveClaimVoucher(){
		Employee emp=getLoginEmployee();
		claimVoucher.setCreator(emp);
		claimVoucher.setDetailList(detailList);
		claimVoucher.setCreateTime(new Date());
		if (claimVoucher.getStatus().equals("新创建")) {
			claimVoucher.setNextDeal(emp);
		}else{
			Employee empManager=getLoginEmployeeManager();
			claimVoucher.setNextDeal(empManager);
		}
		for (ClaimVoucherDetail claimVoucherDetail : detailList) {
			claimVoucherDetail.setBizClaimVoucher(claimVoucher);
		}
		
		claimVoucherService.save(claimVoucher);
		return "save";
	}
	
	public String toCheck(){
		claimVoucher=claimVoucherService.getClaimVoucher(claimVoucher.getId());
		return "tocheck";
	}
	
	
	public String getClaimVoucherById(){
		claimVoucher=claimVoucherService.getClaimVoucher(claimVoucher.getId());
		return "view";
	}
	
	public String toUpdate(){
		claimVoucher=claimVoucherService.getClaimVoucher(claimVoucher.getId());
		return "toupdate";
	}
	
	public String updateClaimVoucher(){
		if (claimVoucher.getStatus().equals("已提交")) {
			claimVoucher.setNextDeal(getLoginEmployeeManager());
		}
		claimVoucher.setDetailList(detailList);
		claimVoucherService.update(claimVoucher);
		return "save";
	}
	
	public String deleteClaimVoucherById(){
		
		claimVoucherService.delete(claimVoucher);
		
		return "save";
	}
	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Map<String, String> getStatusMap() {
		if (getLoginEmployee().getSysPosition().getId()==1) {
			statusMap.put("新创建", "新创建");
			statusMap.put("已提交", "已提交");
			statusMap.put("待审批", "待审批");
			statusMap.put("已打回", "已打回");
			statusMap.put("已终止", "已终止");
		}
		
		if (getLoginEmployee().getSysPosition().getId()==2) {
			statusMap.put("待审批", "待审批");
			statusMap.put("已打回", "已打回");
			statusMap.put("已终止", "已终止");
		}
		
		if (getLoginEmployee().getSysPosition().getId()==3) {
			statusMap.put("待审批", "待审批");
			statusMap.put("已打回", "已打回");
			statusMap.put("已终止", "已终止");
		}
		
		statusMap.put("已审批", "已审批");
		statusMap.put("已付款", "已付款");	
		return statusMap;
	}


	public ClaimVoucher getClaimVoucher() {
		return claimVoucher;
	}


	public void setClaimVoucher(ClaimVoucher claimVoucherl) {
		this.claimVoucher = claimVoucherl;
	}


	public ClaimVoucherService getClaimVoucherService() {
		return claimVoucherService;
	}


	public void setClaimVoucherService(ClaimVoucherService claimVoucherService) {
		this.claimVoucherService = claimVoucherService;
	}

	public List<ClaimVoucherDetail> getDetailList() {
		return detailList;
	}

	public void setDetailList(List<ClaimVoucherDetail> detailList) {
		this.detailList = detailList;
	}
 

}

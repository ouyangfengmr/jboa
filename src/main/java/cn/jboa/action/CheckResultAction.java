package cn.jboa.action;

import cn.jboa.entity.CheckResult;
import cn.jboa.service.CheckResultService;

public class CheckResultAction extends BaseAction{
	private CheckResult checkResult;
	private CheckResultService checkResultService;
	
	public String checkClaimVoucher() {
		checkResult.setCheckEmployee(this.getLoginEmployee());
		checkResultService.saveCheckResult(checkResult);
		return SUCCESS;
	}

	public CheckResult getCheckResult() {
		return checkResult;
	}

	public void setCheckResult(CheckResult checkResult) {
		this.checkResult = checkResult;
	}

	public CheckResultService getCheckResultService() {
		return checkResultService;
	}

	public void setCheckResultService(CheckResultService checkResultService) {
		this.checkResultService = checkResultService;
	}
	
	
	
}

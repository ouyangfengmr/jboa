package cn.jboa.action;

import cn.jboa.common.Constants;
import cn.jboa.entity.Employee;
import cn.jboa.service.EmployeeService;

public class EmployeeAction extends BaseAction{
	
	private EmployeeService employeeService;
	private Employee employee;
	private String msg;
	private Employee manager;
	private String random;

	public String login(){
		Object sessionRandom  = this.getSession().get("random");
		if (random == null || !random.equals(sessionRandom)) {
			this.addActionError(this.getText("errors.codeerror"));
			msg = this.getText("errors.codeerror");
			return INPUT;
		}
		
		//employee.setPassword(MD5.MD5Encode(employee.getPassword()));
		try {
			employee = employeeService.login(employee);
			manager = employeeService.getManager(employee);
			this.getSession().put(Constants.AUTH_EMPLOYEE, employee);
			this.getSession().put(Constants.AUTH_EMPLOYEE_MANAGER, manager);
			this.getSession().put(Constants.EMPLOYEE_POSITION,
					employee.getSysPosition().getNameCn());
		} catch (Exception e) {
			this.addActionError(this
					.getText("errors.invalid.usernameorpassword"));
			msg = this.getText("errors.invalid.usernameorpassword");
		}
		if (this.hasActionErrors())// if (msg != null)
			return INPUT;
		return SUCCESS;

	}
	
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	
	
	public void setEmployeeService(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Employee getManager() {
		return manager;
	}

	public void setManager(Employee manager) {
		this.manager = manager;
	}

	public String getRandom() {
		return random;
	}

	public void setRandom(String random) {
		this.random = random;
	}	
}

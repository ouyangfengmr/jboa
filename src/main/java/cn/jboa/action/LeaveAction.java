package cn.jboa.action;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import cn.jboa.common.Constants;
import cn.jboa.entity.Leave;
import cn.jboa.service.LeaveService;

public class LeaveAction extends BaseAction{
	
	private static Map<String,String> leaveTypeMap;
	private Leave leave;
	private LeaveService leaveService;
	private Date startDate;
	private Date endDate;
	
	static{
		leaveTypeMap =new LinkedHashMap<String, String>();
		leaveTypeMap.put(Constants.LEAVE_ANNUAL, Constants.LEAVE_ANNUAL);
		leaveTypeMap.put(Constants.LEAVE_CASUAL, Constants.LEAVE_CASUAL);
		leaveTypeMap.put(Constants.LEAVE_MARRIAGE, Constants.LEAVE_MARRIAGE);
		leaveTypeMap.put(Constants.LEAVE_MATERNITY, Constants.LEAVE_MATERNITY);
		leaveTypeMap.put(Constants.LEAVE_SICK, Constants.LEAVE_SICK);
		
	}
	
	public String toEdit(){
		return "toadd";
	}
	
	public String saveLeave(){
		leave.setCreator(getLoginEmployee());
		leave.setNextDeal(getLoginEmployeeManager());
		leave.setCreateTime(new Date());
		leave.setStatus("������");
		leaveService.save(leave);
		return "save";
	}
	
	public String searchLeave(){
		pageSupport=leaveService.getLeavePage(getLoginEmployee(), startDate, endDate, pageNo, pageSize);
		return "list";
	}
	
	public String getLeaveById(){
		leave=leaveService.get(leave.getId());
		return "view";
	}
	
	public String toCheck(){
		leave=leaveService.get(leave.getId());
		return "tocheck";
	}
	
	public String checkLeave(){
		leaveService.update(leave);
		return "save";
	}
	
	
	public Map<String,String> getLeaveTypeMap() {
		return leaveTypeMap;
	}
	

	public void setLeaveTypeMap(Map<String,String> leaveTypeMap) {
		LeaveAction.leaveTypeMap = leaveTypeMap;
	}

	public Leave getLeave() {
		return leave;
	}

	public void setLeave(Leave leave) {
		this.leave = leave;
	}

	public LeaveService getLeaveService() {
		return leaveService;
	}

	public void setLeaveService(LeaveService leaveService) {
		this.leaveService = leaveService;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	
}

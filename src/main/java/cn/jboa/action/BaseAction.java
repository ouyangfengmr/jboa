package cn.jboa.action;

import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import cn.jboa.common.Constants;
import cn.jboa.entity.Employee;
import cn.jboa.util.PaginationSupport;

public class BaseAction<T> extends ActionSupport {
	
	protected Integer pageNo=1;
	protected Integer pageSize =5;
	protected PaginationSupport<T> pageSupport;
	
	protected Map<String, Object> getSession() {
		ActionContext actionContext = ActionContext.getContext();
		return actionContext.getSession();
	}
	
	protected Employee getLoginEmployee(){
		return (Employee) getSession().get(Constants.AUTH_EMPLOYEE);
	}
	
	protected Employee getLoginEmployeeManager(){
		return (Employee) getSession().get(Constants.AUTH_EMPLOYEE_MANAGER);
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public PaginationSupport<T> getPageSupport() {
		return pageSupport;
	}

	public void setPageSupport(PaginationSupport<T> pageSupport) {
		this.pageSupport = pageSupport;
	}
	
	
	
}

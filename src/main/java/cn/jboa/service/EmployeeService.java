package cn.jboa.service;

import cn.jboa.entity.Employee;

public interface EmployeeService {
	public Employee login(Employee emp);
	
	public Employee getManager(Employee employee);
}

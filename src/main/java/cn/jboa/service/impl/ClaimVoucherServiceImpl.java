package cn.jboa.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cn.jboa.dao.ClaimVoucherDao;
import cn.jboa.dao.ClaimVoucherDetailDao;
import cn.jboa.entity.ClaimVoucher;
import cn.jboa.entity.ClaimVoucherDetail;
import cn.jboa.entity.Employee;
import cn.jboa.service.ClaimVoucherService;
import cn.jboa.util.PaginationSupport;

@Repository("claimVoucherService")
public class ClaimVoucherServiceImpl implements ClaimVoucherService{
	
	@Autowired
	private ClaimVoucherDao claimVoucherDao;
	
	@Autowired
	private ClaimVoucherDetailDao claimVoucherDetailDao;
	
	//查询员工所有报销单
	public PaginationSupport<ClaimVoucher> getClaimVoucherPage(Employee emp, String status, Date startDate,
			Date endDate, Integer pageNo, Integer pageSize) {
		
		PaginationSupport<ClaimVoucher> result = new PaginationSupport<ClaimVoucher>();
		
		if (pageNo>0) {
			result.setCurrPageNo(pageNo);
		}
		
		if (pageSize>0) {
			result.setPageSize(pageSize);
		}
		
		StringBuilder hqlBuilder = new StringBuilder("from ClaimVoucher where  ");
		List<Object> values = new ArrayList<Object>();
		
		if (emp.getSysPosition().getId()==1) {
			hqlBuilder.append("creator.sn=?");
			values.add(emp.getSn());
		}else if(emp.getSysPosition().getId()==2){
			hqlBuilder.append("creator.sysDepartment.id=?");
			values.add(emp.getSysDepartment().getId());
		}else if(emp.getSysPosition().getId()==3){
			hqlBuilder.append("1=1");
		}else{
			hqlBuilder.append("nextDeal.sn=?");
			values.add(emp.getSn());
		}
		
		if (status !=null && status.length() !=0) {
			hqlBuilder.append("and status =?");
			values.add(status);
		}else{
			if (emp.getSysPosition().getId()==2) {
				hqlBuilder.append("and status is not '新创建' ");
			}else if(emp.getSysPosition().getId()==3){
				hqlBuilder.append("and status = '待审批' ");
			}else if(emp.getSysPosition().getId()==4){
				hqlBuilder.append("and status = '已审批' ");
			}else{
				
			}
			
		}
			
		if (startDate != null) {
			hqlBuilder.append(" and createTime >= ?");// yyyy-MM-dd 00:00:00
			values.add(startDate);
		}
		if (endDate != null) {
			hqlBuilder.append(" and createTime < ?");// yyyy-MM-dd 00:00:00
			Calendar oneDayLater = Calendar.getInstance();
			oneDayLater.setTime(endDate);
			oneDayLater.add(Calendar.DAY_OF_MONTH, 1);
			values.add(oneDayLater.getTime());
		}

		int count = claimVoucherDao.getTotalCount(hqlBuilder.toString(),
				values.toArray()).intValue();
		result.setTotalCount(count);

		if (count > 0) {
			// count % pageSize == 0 ? count / pageSize : (count / pageSize) + 1
			int pageCount = (count + result.getPageSize() - 1)
					/ result.getPageSize();
			result.setTotalPageCount(pageCount);
			if (result.getCurrPageNo() > pageCount)
				result.setCurrPageNo(pageCount);

			if (status == null || status.length() == 0)
				hqlBuilder.append(" order by status, createTime desc");
			else
				hqlBuilder.append(" order by createTime desc");
			
			List<ClaimVoucher> items = claimVoucherDao.findForPage(hqlBuilder.toString(),
					result.getCurrPageNo(), result.getPageSize(),
					values.toArray());
			result.setItems(items);
		}

		
		return result;
	}

	public void save(ClaimVoucher claimVoucher) {
		claimVoucherDao.save(claimVoucher);
	}

	public ClaimVoucher getClaimVoucher(Long id) {
		return claimVoucherDao.get(id);
	}

	public void update(ClaimVoucher claimVoucher) {
		
		ClaimVoucher claimVoucherUpdate=claimVoucherDao.get(claimVoucher.getId());
		claimVoucherUpdate.setStatus(claimVoucher.getStatus());
		claimVoucherUpdate.setModifyTime(new Date());
		
		for (ClaimVoucherDetail cd : claimVoucherUpdate.getDetailList()) {
			claimVoucherDetailDao.delete(cd);
		}
		
		claimVoucherUpdate.setTotalAccount(claimVoucher.getTotalAccount());
		claimVoucherUpdate.setDetailList(claimVoucher.getDetailList());
		for (ClaimVoucherDetail d : claimVoucherUpdate.getDetailList()) {
			d.setBizClaimVoucher(claimVoucherUpdate);
		}
		claimVoucherUpdate.setNextDeal(claimVoucher.getNextDeal());
		claimVoucherUpdate.setEvent(claimVoucher.getEvent());
		
		claimVoucherDao.update(claimVoucherUpdate);
	}

	public void delete(ClaimVoucher claimVoucher) {
		claimVoucher=claimVoucherDao.get(claimVoucher.getId());
		
		for (ClaimVoucherDetail cd : claimVoucher.getDetailList()) {
			claimVoucherDetailDao.delete(cd);
		}
		
		claimVoucherDao.delete(claimVoucher);
	}

}

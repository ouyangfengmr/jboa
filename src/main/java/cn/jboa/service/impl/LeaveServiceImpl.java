package cn.jboa.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cn.jboa.dao.LeaveDao;
import cn.jboa.entity.Employee;
import cn.jboa.entity.Leave;
import cn.jboa.service.LeaveService;
import cn.jboa.util.PaginationSupport;

@Repository("leaveService")
public class LeaveServiceImpl implements LeaveService{

	@Autowired
	private LeaveDao leaveDao;

	public void save(Leave leave) {
		leaveDao.save(leave);
	}

	public PaginationSupport<Leave> getLeavePage(Employee emp, Date startDate, Date endDate, Integer pageNo,
			Integer pageSize) {
		PaginationSupport<Leave> result = new PaginationSupport<Leave>();

		if (pageNo>0) {
			result.setCurrPageNo(pageNo);
		}

		if (pageSize>0) {
			result.setPageSize(pageSize);
		}
		
		StringBuilder hqlBuilder = new StringBuilder("from Leave where  ");
		List<Object> values = new ArrayList<Object>();
		
		if (emp.getSysPosition().getId()==1) {
			hqlBuilder.append("creator.sn=? ");
			values.add(emp.getSn());
		}else if(emp.getSysPosition().getId()==2){
			hqlBuilder.append("creator.sysDepartment.id=?");
			values.add(emp.getSysDepartment().getId());
		}else if(emp.getSysPosition().getId()==3){
			hqlBuilder.append("1=1");
		}
		
		if (startDate != null) {
			hqlBuilder.append(" and createTime >= ?");
			values.add(startDate);
		}
		if (endDate != null) {
			hqlBuilder.append(" and createTime < ?");
			Calendar oneDayLater = Calendar.getInstance();
			oneDayLater.setTime(endDate);
			oneDayLater.add(Calendar.DAY_OF_MONTH, 1);
			values.add(oneDayLater.getTime());
		}

		int count = leaveDao.getTotalCount(hqlBuilder.toString(),
				values.toArray()).intValue();
		result.setTotalCount(count);

		if (count > 0) {
			// count % pageSize == 0 ? count / pageSize : (count / pageSize) + 1
			int pageCount = (count + result.getPageSize() - 1)
					/ result.getPageSize();
			result.setTotalPageCount(pageCount);
			if (result.getCurrPageNo() > pageCount)
				result.setCurrPageNo(pageCount);

				hqlBuilder.append(" order by createTime desc");

			List<Leave> items = leaveDao.findForPage(hqlBuilder.toString(),
					result.getCurrPageNo(), result.getPageSize(),
					values.toArray());
			result.setItems(items);
		}
		return result;
	}

	public Leave get(Long id) {
		return leaveDao.get(id);
	}

	public void update(Leave leave) {
		Leave updateLeave=get(leave.getId());
		updateLeave.setModifyTime(new Date());
		updateLeave.setStatus(leave.getStatus());
		updateLeave.setApproveOpinion(leave.getApproveOpinion());
		updateLeave.setNextDeal(null);
		
		leaveDao.update(updateLeave);
	}

}

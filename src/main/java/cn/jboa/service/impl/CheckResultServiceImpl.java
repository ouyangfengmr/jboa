package cn.jboa.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cn.jboa.common.Constants;
import cn.jboa.dao.CheckResultDao;
import cn.jboa.dao.ClaimVoucherDao;
import cn.jboa.dao.EmployeeDao;
import cn.jboa.entity.CheckResult;
import cn.jboa.entity.ClaimVoucher;
import cn.jboa.service.CheckResultService;

@Repository("checkResultService")
public class CheckResultServiceImpl implements CheckResultService{
	
	@Autowired
	private CheckResultDao checkResultDao;
	
	@Autowired
	private ClaimVoucherDao claimVoucherDao;
	
	@Autowired
	private EmployeeDao employeeDao;

	
	public void saveCheckResult(CheckResult checkResult) {
		checkResult.setCheckTime(new Date());
		checkResultDao.save(checkResult);

		ClaimVoucher claimVoucher = claimVoucherDao.get(checkResult
				.getClaimId());
		claimVoucher.setModifyTime(checkResult.getCheckTime());
		this.updateClaimVoucherStatus(checkResult.getCheckEmployee()
				.getSysPosition().getNameCn(), checkResult.getResult(),
				claimVoucher);

	}
	
	protected void updateClaimVoucherStatus(String position,
			String checkResult, ClaimVoucher claimVoucher) {
		if (Constants.CHECKRESULT_PASS.equals(checkResult)) {
			if (Constants.POSITION_FM.equals(position)) {
				if (claimVoucher.getTotalAccount() >= 5000) {
					claimVoucher.setStatus("待审批");
					claimVoucher.setNextDeal(employeeDao.getGeneralManager());
				}
				else {
					claimVoucher.setStatus("已审批");
					claimVoucher.setNextDeal(employeeDao.getCashier());
				}
			}
			else if (Constants.POSITION_GM.equals(position)) {
				claimVoucher.setStatus("已审批");
				claimVoucher.setNextDeal(employeeDao.getCashier());
			}
			else if (Constants.POSITION_CASHIER.equals(position)) {
				claimVoucher.setStatus("已付款");
				claimVoucher.setNextDeal(null);
			}
		}
		else if (Constants.CHECKRESULT_REJECT.equals(checkResult)) {
			claimVoucher.setStatus("已终止");
			claimVoucher.setNextDeal(null);
		}
		else if (Constants.CHECKRESULT_BACK.equals(checkResult)) {
			claimVoucher.setStatus("已打回");
			claimVoucher.setNextDeal(claimVoucher.getCreator());
		}
	}


}

package cn.jboa.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cn.jboa.dao.EmployeeDao;
import cn.jboa.entity.Employee;
import cn.jboa.service.EmployeeService;

@Repository("employeeService")
public class EmployeeServiceImpl implements EmployeeService {
	
	@Autowired
	private EmployeeDao employeeDao;
	
	public Employee login(Employee emp) {
		Employee employee = employeeDao.fineEmployeeBySN(emp.getSn());
		if (employee != null && employee.getPassword().equals(emp.getPassword())) {
			return employee;
		} else {
			try {
				throw new Exception("出现异常");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	public Employee getManager(Employee employee) {
		return employeeDao.getManager(employee);
	}
	
}

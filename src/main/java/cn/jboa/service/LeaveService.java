package cn.jboa.service;

import java.util.Date;

import cn.jboa.entity.Employee;
import cn.jboa.entity.Leave;
import cn.jboa.util.PaginationSupport;

public interface LeaveService {
	public void save(Leave leave);
	
	public Leave get(Long id);
	
	public void update(Leave leave);
	
	public PaginationSupport<Leave> getLeavePage(Employee emp,Date startDate,Date endDate,Integer pageNo,Integer pageSize);
}

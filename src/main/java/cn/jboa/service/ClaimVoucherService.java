package cn.jboa.service;

import java.util.Date;

import cn.jboa.entity.ClaimVoucher;
import cn.jboa.entity.Employee;
import cn.jboa.util.PaginationSupport;

public interface ClaimVoucherService {
	
	public PaginationSupport<ClaimVoucher> getClaimVoucherPage(Employee emp,String status,Date startDate,Date endDate,Integer pageNo,Integer pageSize);
	
	public void save(ClaimVoucher claimVoucher);
	
	public ClaimVoucher getClaimVoucher(Long id);
	
	public void update(ClaimVoucher claimVoucher);
	
	public void delete(ClaimVoucher claimVoucher);
	
}

﻿package cn.jboa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 员工类
 * 
 * @author Administrator
 *
 */
public class Employee {
	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public Position getSysPosition() {
		return sysPosition;
	}

	public void setSysPosition(Position sysPosition) {
		this.sysPosition = sysPosition;
	}

	public Department getSysDepartment() {
		return sysDepartment;
	}

	public void setSysDepartment(Department sysDepartment) {
		this.sysDepartment = sysDepartment;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	// 员工id
	private String sn;
	// 角色对象 多个员工对应一个角色
	private Position sysPosition;
	// 部门对象
	private Department sysDepartment;
	// 员工名称
	private String name;
	// 密码
	private String password;
	// 状态
	private String status;
}
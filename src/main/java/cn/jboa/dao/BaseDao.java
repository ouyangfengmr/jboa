package cn.jboa.dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public interface BaseDao<T> {
	public void save(T instance);
	
	public void update(T instance);
	
	public void saveOrUpdate(T instance);
	
	public T merge(T instance);
	
	public void delete(T instance);
	
	public void delete(Collection<T> instances);
	
	public Integer delete(Object[] ids); // Class<T> cls, 
	
	public T get(Serializable id); //Class<T> cls, 
	
	public List<T> findAll(); // from Xxx Class<T> cls
	
	// 投影查询，自定义
	// List<User> users = findForPage()
	// List<Order> orders = findForPage()
	public <E> List<E> findForPage(String hql, int pageNo, int pageSize, Object... values); // ?
//	public <E> List<E> findForPage(String hql, int pageNo, int pageSize);
//	public <E> List<E> findForPage(String hql, int pageNo, int pageSize, Object value); // :属性名
//	public <E> List<E> findForPage(String hql, int pageNo, int pageSize, Map<String, Object> values); // :key
//	public <E> List<E> findForPage(String hql, int pageNo, int pageSize, Object[] values); // ?
	
	public Number getTotalCount(String hql, Object... values);
	
	public <E> List<E> find(String hql, Object... values);
}

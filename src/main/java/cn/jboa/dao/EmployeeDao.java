package cn.jboa.dao;

import java.util.List;

import cn.jboa.entity.Employee;

public interface EmployeeDao {
	
	public Employee fineEmployeeBySN(String sn);
	
	public Employee getManager(Employee employee);

	public Employee getGeneralManager();

	public Employee getCashier();
}

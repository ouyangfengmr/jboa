package cn.jboa.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cn.jboa.dao.LeaveDao;
import cn.jboa.entity.Leave;

@Repository("leaveDao")
public class LeaveDaoImpl extends BaseHibernateDaoSupport<Leave> implements LeaveDao{
	public LeaveDaoImpl(){

	}

	@Autowired
	public LeaveDaoImpl(SessionFactory sessionFactory){
		this.setSessionFactory(sessionFactory);
	}
}

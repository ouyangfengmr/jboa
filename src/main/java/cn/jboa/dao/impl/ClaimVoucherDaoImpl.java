package cn.jboa.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cn.jboa.dao.ClaimVoucherDao;
import cn.jboa.entity.ClaimVoucher;

@Repository("claimVoucherDao")
public class ClaimVoucherDaoImpl extends BaseHibernateDaoSupport<ClaimVoucher>
implements ClaimVoucherDao {
	public ClaimVoucherDaoImpl(){

	}

	@Autowired
	public ClaimVoucherDaoImpl(SessionFactory sessionFactory){
		this.setSessionFactory(sessionFactory);
	}
}

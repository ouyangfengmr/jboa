package cn.jboa.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cn.jboa.dao.ClaimVoucherDetailDao;
import cn.jboa.entity.ClaimVoucherDetail;

@Repository("claimVoucherDetailDao")
public class ClaimVoucherDetailDaoImpl extends BaseHibernateDaoSupport<ClaimVoucherDetail> implements ClaimVoucherDetailDao{
	public ClaimVoucherDetailDaoImpl(){

	}

	@Autowired
	public ClaimVoucherDetailDaoImpl(SessionFactory sessionFactory){
		this.setSessionFactory(sessionFactory);
	}
}

package cn.jboa.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cn.jboa.common.Constants;
import cn.jboa.dao.EmployeeDao;
import cn.jboa.entity.Employee;

@Repository("employeeDao")
public class EmployeeDaoImpl extends BaseHibernateDaoSupport<Employee>
		implements EmployeeDao {
	
	public EmployeeDaoImpl(){
		
	}
	
	@Autowired
	public EmployeeDaoImpl(SessionFactory sessionFactory){
		this.setSessionFactory(sessionFactory);
	}
	public Employee fineEmployeeBySN(String sn) {
		String hql = "from Employee e where e.sn = ?";
		@SuppressWarnings("unchecked")
		List<Employee> result = this.getHibernateTemplate().find(hql, sn);
		return (result == null || result.size() == 0) ? null : result.get(0);
	}

	public Employee getManager(Employee employee) {
		String hql = "from Employee e where e.status = ? and e.sysDepartment.id = ? and e.sysPosition.nameCn = ?";
		List<Employee> emps = this.find(hql, Constants.EMPLOYEE_STAY, employee
				.getSysDepartment().getId(), Constants.POSITION_FM);
		if (emps == null || emps.size() == 0)
			return null;
		return emps.get(0);
	}

	public Employee getGeneralManager() {
		String hql = "from Employee  where sysPosition.id = 3";
		List<Employee> result = this.getHibernateTemplate().find(hql);
		return (result == null || result.size() == 0) ? null : result.get(0);
	}

	public Employee getCashier() {
		String hql = "from Employee e where e.sn = 004";
		List<Employee> result = this.getHibernateTemplate().find(hql);
		return (result == null || result.size() == 0) ? null : result.get(0);
	}
}

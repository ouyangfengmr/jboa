package cn.jboa.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cn.jboa.dao.CheckResultDao;
import cn.jboa.entity.CheckResult;

@Repository("checkResultDao")
public class CheckResultDaoImpl extends BaseHibernateDaoSupport<CheckResult> implements CheckResultDao{
	public CheckResultDaoImpl(){

	}

	@Autowired
	public CheckResultDaoImpl(SessionFactory sessionFactory){
		this.setSessionFactory(sessionFactory);
	}
}

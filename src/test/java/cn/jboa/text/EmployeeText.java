package cn.jboa.text;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.jboa.entity.ClaimVoucher;
import cn.jboa.entity.Employee;
import cn.jboa.service.ClaimVoucherService;
import cn.jboa.service.EmployeeService;

public class EmployeeText {
	
	@Test
	public void textList(){
	}
	
	@Test
	public void login(){
		ApplicationContext ctx=new ClassPathXmlApplicationContext("applicationContext.xml");
		EmployeeService employeeService = (EmployeeService) ctx.getBean("employeeService");
		
		Employee employee= new Employee();
		employee.setSn("001");
		employee.setPassword("123");
		
		Employee employeeSession =employeeService.login(employee);
		
		if (employeeSession==null) {
			System.out.println("登入失败");
		}else{
			System.out.println("登入成功");
		}
	}
	
	@Test
	public void save222(){
		ApplicationContext ctx=new ClassPathXmlApplicationContext("applicationContext.xml");
		ClaimVoucherService claimVoucherService =(ClaimVoucherService) ctx.getBean("claimVoucherService");
		ClaimVoucher cl = new ClaimVoucher();
		
		cl.setId(222L);
		cl.setCreateTime(new Date());
		cl.setStatus("新创建");
		claimVoucherService.save(cl);
	}
}
